readme.txt for CS494 Week 4 assignment

Name:     C. Vaughan Schmidt  
email:    schmicar@onid.oregonstate.edu
Course:   CS494

assignment notes:

1.  For extra work, extra effort made on design & layout of weather page

2.  Note that as of the time this readme is made, the individual divs jump out
    of allignment with each other depending on what content is populated. I'm
    still working on figuring out what is happening there, so may get that 
    looking better (or at least more consistent).

3.  I handle the requested Openweathermap.org calls as Ajax request per 
    instructions.

    -BUT-

    I have also added a callback to the weatherunderground.com service.
    Because wu does not allow cross-domain ajax requests, I had to do this
    one by generating a custom script that the .js file will put in the 
    document header based on city, state - the the json infomation from wu
    is retrieved.

    The we data is not used for very much on the website, other than display 
    a subset of it in raw JSON on a button click (it has a lot more stuff, but
    I just focused on main weather.)

    Also, I am able to grab the weather icon from wu and incorporate onto page
    after the wu call has been triggered.

4.  links to validators at bottom of page for convenience.