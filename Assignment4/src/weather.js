// Name:        C. Vaughan Schmidt
// email:       schmicar@onid.oregonstate.edu
// Course:      CS494 - Fall 2014
// Assignment:  Week 4 - AJAX and localStorage


// For expediency, this file began life as a copy of studentlibrary.js
// Have not focused on removing anything not needed for weather station.

// NOTE: I have not really figured out JSdoc yet, but making a stab at it.

window.onload = function() {

  // retrieve saved settings
  loadSettings();

  // locaStorage checks initialize
  if (localStorage.getItem('Canary')) {
    testResults.innerText = 'Confirmed';
    testResults.style.background = 'green';
    ajaxRequestOWM();
  }
  else {
    testResults.innerText = 'Unknown';
    document.getElementById('testResults').style.background = '#aa9739';
  }

  // button events
  document.getElementsByName('save')[0].onclick =
    function() { saveSettings() };
  document.getElementsByName('OWMweather')[0].onclick =
    function() { ajaxRequestOWM() };
  document.getElementsByName('WUweather')[0].onclick =
    function() { ajaxRequestWU() };
};

/* --------------------------------------------------------------------- */
/** function ajaxRequestOWM()
 *
 * Makes a ajax request to retrieve weather info
 * some credit for this section to Ajax video lecture on Blackboard.
 * openweathermap.org request
 */
function ajaxRequestOWM() {
  var owmReq = new XMLHttpRequest();
  if (!owmReq) {
    throw 'Unable and/or unwilling to create XMLHttpRequest owmReq';
  }

  owmReq.onreadystatechange = loadOWMtoDOM;
  var owmURL = 'http://api.openweathermap.org/data/2.5/weather/?q=';
  owmURL = owmURL + document.getElementsByName('city')[0].value + ',' +
  document.getElementsByName('state')[0].value;
  console.log(owmURL);
  owmReq.open('GET', owmURL);
  owmReq.send(null);
}


/* --------------------------------------------------------------------- */
/** function ajaxRequestWU()
 *
 * Makes a ajax request to retrieve weather info from WEATHER UNDERGROUND
 */
function ajaxRequestWU() {
  // some credit for this section to Ajax video lecture on Blackboard.

  // weatherunderground.com request
  // Note: Handled with a JSONP, build script in document header
  // script execution will callback the processWUJSON()
  var wuReq = new XMLHttpRequest();
  if (!wuReq) {
    throw 'Unable and/or unwilling to create XMLHttpRequest wuReq';
  }
  var wuKey = 'f7ec8190c3e1c837';
  var wuURL = 'http://api.wunderground.com/api/' + wuKey + '/geolookup/' +
    'conditions/q/' + document.getElementsByName('state')[0].value +
    '/' + document.getElementsByName('city')[0].value +
    '.json?callback=processWUJSON';
  console.log('wuURL: ' + wuURL);
  var myWUscript = document.createElement('script');
  myWUscript.setAttribute('src', wuURL);
  document.head.appendChild(myWUscript);
}

/* -----------------------------------------------------------------------*/
/** processWUJSON()
 *
 * Special handler for dealing with a JSONP request to Weatherunderground.com
 * @param {json} json
 */
function processWUJSON(json) {
  console.log(json);
  document.getElementById('rawJSON').innerText =
  JSON.stringify(json.current_observation, null, 2);

  console.log(json.current_observation.icon_url);
  document.body.style.backgroundImage =
  'url(\'' + json.current_observation.icon_url + '\')';
}

/* --------------------------------------------------------------------- */
/** localStorageExits( )
 *
 * Function tests if local storage is accessible in client browser
 * @return {boolean} exists
 */
function localStorageExists() {
  console.log('inside yer localStorage()');
  var exists = false; // local storage assumed false until proven
  var element = document.getElementById('testResults');
  localStorage.setItem('Canary', 'alive');
  if (localStorage.getItem('Canary') === 'alive') {
    exists = true;
    element.innerText = 'Confirmed';
    element.style.background = 'green';
  } else {
    element.innerText = 'No Storage';
    element.style.background = 'red';
  }
  storageTested = true;
  return exists;
}

/* ------------------------------------------------------------------------- */
/** localStorageClear( )
 *
 * Function clears local storage if request and forces reload
 */
function localStorageClear() {
  window.localStorage.clear();
  location.reload(true);
}

/* ------------------------------------------------------------------------- */
/** saveSettings()
 *
 * Function Saves user selections to local storage
 * @return {boolean} was save success
 */
function saveSettings() {
  if (localStorageExists()) {
    localStorage.setItem('city',
      document.getElementsByName('city')[0].value);
    localStorage.setItem('state',
      document.getElementsByName('state')[0].value);
    localStorage.setItem('wind',
      document.getElementsByName('refinements')[0].checked);
    localStorage.setItem('curTemp',
      document.getElementsByName('refinements')[1].checked);
    localStorage.setItem('minTemp',
      document.getElementsByName('refinements')[2].checked);
    localStorage.setItem('maxTemp',
      document.getElementsByName('refinements')[3].checked);
    return true;
  }
  return false;
}

/* ------------------------------------------------------------------------- */
/** compassDir(deg)
 *
 * Function Saves user selections to local storage
 * @param {int} deg wind direction
 * @return {string} compass direction
 *
 * Based on:
 * http://codereview.stackexchange.com/questions/1287/cleaner-way-to-calculate-compass-direction
 */
function compassDir(deg) {
  var directions = ['N', 'NNE', 'NE', 'ENE',
        'E', 'ESE', 'SE', 'SSE',
        'S', 'SSW', 'SW', 'WSW',
        'W', 'WNW', 'NW', 'NNW'];

  var index = Math.round((deg + 12) / 23);
  console.log('wind direction: ' + deg + ' = ' + index);
  return directions[index];
}

/* ------------------------------------------------------------------------- */

function loadSettings() {
  // retrieve localStorage values for checkboxes & fields
  if (localStorage.city) {
    document.getElementById('city').value = localStorage.city;
  }
  if (localStorage.state) {
    document.getElementById('state').value = localStorage.state;
  }
  if (localStorage.wind === 'true') {
    document.getElementsByName('refinements')[0].checked = true;
  }
  if (localStorage.curTemp === 'true') {
    document.getElementsByName('refinements')[1].checked = true;
  }
  if (localStorage.minTemp === 'true') {
    document.getElementsByName('refinements')[2].checked = true;
  }
  if (localStorage.maxTemp === 'true') {
    document.getElementsByName('refinements')[3].checked = true;
  }

  // if (localStorage.wind && localStorage.curTemp && localStorage.minTemp &&
  //   localStorage.maxTemp) {
  //  document.getElementById('all')[0] = true;
  // }
}

/* ------------------------------------------------------------------------- */
/** loadOWMtoDOM
 *
 * Function loads the OpenWeatherMap.org info to DOM
 * @this is called from owmrequest object on state change
 */
function loadOWMtoDOM() {
  console.log('Ready State: ' + this.readyState + ' / status: ' + this.status);
  console.log(this);
  try {
    if (this.readyState === 4 && this.status === 200) {
      var parsed = JSON.parse(this.responseText);
      var curTemp = Math.round((parsed.main.temp - 273.15) * 1.8 + 32);
      var minTemp = Math.round((parsed.main.temp_min - 273.15) * 1.8 + 32);
      var maxTemp = Math.round((parsed.main.temp_max - 273.15) * 1.8 + 32);
      var date = new Date(parsed.dt * 1000);
      var wind = parsed.wind.speed;
      var dir = compassDir(parsed.wind.deg);
      var cond = parsed.weather[0].main;
      var humidity = parsed.main.humidity;
      console.log(parsed.weather[0].main);
      console.log(date);
    }
  }

  catch (e) {
    alert('Exception caught in loadOWMtoDOM: ' + e.description);
  }

  var empty = '&nbsp;';

  if (document.getElementsByName('refinements')[1].checked) {
    document.getElementById('cur').innerHTML = curTemp + ' F';
  }
  else {
    document.getElementById('cur').innerHTML = empty;
  }

  if (document.getElementsByName('refinements')[2].checked) {
    document.getElementById('min').innerHTML = minTemp + ' F';
  }
  else {
    document.getElementById('min').innerHTML = empty;
  }

  if (document.getElementsByName('refinements')[3].checked) {
    document.getElementById('max').innerHTML = maxTemp + ' F';
  }
  else {
    document.getElementById('max').innerHTML = empty;
  }

  console.log('Wind checked: ' +
  document.getElementsByName('refinements')[0].checked);

  if (document.getElementsByName('refinements')[0].checked) {
    document.getElementById('windSpeed').innerHTML = Math.round(wind * 2.369);
    document.getElementById('windDir').innerHTML = dir;
  } else {
    document.getElementById('windSpeed').innerHTML = empty;
    document.getElementById('windDir').innerHTML = empty;
  }

  document.getElementById('humidity').innerHTML = humidity + '%';
  document.getElementById('currCond').innerHTML = cond;
  document.getElementById('currHead').innerHTML = 'As of ' +
    date;
  document.getElementById('location').innerHTML =
  document.getElementById('city').value + ', ' +
  document.getElementById('state').value;
}



