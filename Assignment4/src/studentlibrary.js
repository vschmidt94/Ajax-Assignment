// Name:        C. Vaughan Schmidt
// email:       schmicar@onid.oregonstate.edu
// Course:      CS494 - Fall 2014
// Assignment:  Week 4 - AJAX and localStorage


//Your library code goes in this file.

// NOTE: I have not really figured out JSdoc 100% yet, but making a stab at it.

/** function ajaxRequest
 *
 * @param {string} URL
 * @param {string} Type
 * @param {string} Parameters
 * @return  {
 *          'success': boolean,
 *          'code': string,
 *          'codeDetail': string,
 *          'response': string
 *          }
 */
function ajaxRequest(URL, Type, Parameters) {
  // some credit for this section to Ajax video lecture on Blackboard.

  req = new XMLHttpRequest();
  if (!req) {
    throw 'Unable and/or unwilling to create XMLHttpRequest';
  }

  var retObj = {
    'success': null,
    'code': null,
    'codeDetail': null,
    'response': null
  };

  // Prepare GET or POST request details
  var url = URL;    // local URL in case of GET
  var payload = urlStringify(Parameters);
  // Set up GET or POST details
  if (Type === 'GET') {
    url = url + '?' + payload;
    console.log('url: ' + url);
  } else if (Type !== 'POST') {
    // it's not GET or POST, throw error
    throw 'HTTP request type is not GET or POST.';
  }

  // AJAX HTTP Request
  if (Type === 'POST') {
    req.open(Type, url);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(payload);
  } else if (Type === 'GET') {
    req.open(Type, url);
    req.send(null);
  }

  req.onreadystatechange = function() {
    if (req.readyState === 4) {
      if (req.status >= 200 && req.status < 300) {
        retObj.success = true;
      } else {
        retObj.success = false;
      }
      retObj.code = req.status;
      retObj.codeDetail = req.statusText;
      retObj.response = req.responseText;
    }
  };

  return retObj;
}


/** localStorageExits( )
 *
 * Function tests if local storage is accessible in client browser
 * @return {boolean} exists
 */
function localStorageExists() {
  // console.log('inside localStorage()');
  var exists = false; // local storage assumed false until proven
  localStorage.setItem('Canary', 'alive');
  var element = document.getElementById('testResults');
  if (localStorage.getItem('Canary') === 'alive') {
    exists = true;
  }
  return exists;
}

/** localStorageClear( )
 *
 * Function clears local storage if request and forces reload
 */
function localStorageClear() {
  window.localStorage.clear();
  location.reload(true);
}


/** urlStringify(obj)
 *
 * Function converts array object into a string suitable for HTTP GET request.
 * @param {array object} obj
 * @return {string} URI encoded string
 *
 * CREDIT FOR THIS CODE TO AJAX VIDEO LECTURE ON CS494 BLACKBOARD
 */
function urlStringify(obj) {
  var str = [];
  for (var prop in obj) {
    var s = encodeURIComponent(prop) + '=' + encodeURIComponent(obj[prop]);
    str.push(s);
  }
  return str.join('&');
}
